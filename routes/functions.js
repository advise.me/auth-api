const express = require('express');
const router = express.Router();
const Function = require('../models/function');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

router.get('/all', (req, res, next) => {
  Function.getAllFunctions((err, functions) => {
    return res.json({functions})
  });
});

module.exports = router;
