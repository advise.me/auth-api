const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const mongoose = require('mongoose');


router.get('/all', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  User.getAllUsers((err, users) => {
    return res.json({users})
  });
});

router.put('/update/password', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  User.comparePassword(req.body.oldPassword, req.user.password, (err, isMatch) => {
    if(err) throw err;
    if(isMatch) {
      User.updatePassword(req.user, req.body.password, (err, user) => {
        if(err) {
          res.json({success: false, msg:err});
        } else {
          res.json({success: true, msg:'User password updated'});
        }
      });
    } else {
      return res.json({success: false, msg: 'Wrong password'});
    }
  });
});

// Register
router.post('/register', (req, res, next) => {
  let newUser = new User({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
    function:req.body.function,
    organism:req.body.organism
  });

  User.addUser(newUser, (err, user) => {
    if(err) {
      res.json({success: false, msg:err});
    } else {
      res.json({success: newUser, msg:'User registered'});
    }
  });
});

// Authenticate
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user) {
      return res.json({success: false, msg: 'User not found'});
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;
      if(isMatch){
        const token = jwt.sign({data: user}, config.secret, {
          expiresIn: 604800 // session expires in 1 week
        });

        res.json({
          success: true,
          token: 'JWT ' + token,
          user: {
            id: user._id,
            firstname: user.firstname,
            lastname: user.lastname,
            username: user.username,
            email: user.email,
            function: user.function,
            organism: user.organism
          }
        });
      } else {
        return res.json({success: false, msg: 'Wrong password'});
      }
    });
  });
});


// Profile
router.get('/profile', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  res.json({user: req.user});
});

// Update User
router.put('/update', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  User.updateUser(req.user, req.body, (err, user) => {
    if(err) {
      res.json({success: false, msg:err});
    } else {
      res.json({success: true, msg:'User updated'});
    }
  });
});

// Update user password
router.put('/update/password', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  User.comparePassword(req.body.oldPassword, req.user.password, (err, isMatch) => {
    if(err) throw err;
    if(isMatch) {
      User.updatePassword(req.user, req.body.password, (err, user) => {
        if(err) {
          res.json({success: false, msg:err});
        } else {
          res.json({success: true, msg:'User password updated'});
        }
      });
    } else {
      return res.json({success: false, msg: 'Wrong password'});
    }
  });
});

module.exports = router;
