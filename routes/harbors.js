const express = require('express');
const router = express.Router();
const Function = require('../models/harbor');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

router.get('/all', (req, res, next) => {
  Function.getAllHarbors((err, harbors) => {
    return res.json({harbors})
  });
});

module.exports = router;
