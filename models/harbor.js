const mongoose = require('mongoose');
const config = require('../config/database');

// Function schema
const HarborSchema = mongoose.Schema({
  latitude: {
    type: Number,
    required: true
  },
  longitude: {
    type: Number,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  }
});

const Harbor = module.exports = mongoose.model('Harbor', HarborSchema);

module.exports.getAllHarbors = function(callback) {
  Harbor.find(callback);
}
