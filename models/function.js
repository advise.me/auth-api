const mongoose = require('mongoose');
const config = require('../config/database');

// Function schema
const FunctionSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  }
});

const Function = module.exports = mongoose.model('Function', FunctionSchema);

module.exports.getAllFunctions = function(callback) {
  Function.find(callback);
}
