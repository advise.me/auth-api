const mongoose = require('mongoose');
const schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const config = require('../config/database');

// User schema
const UserSchema = mongoose.Schema({
  _id: {
    type: schema.Types.ObjectId
  },
  firstname: {
    type: String
  },
  lastname: {
    type: String
  },
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  organism: {
    type: String
  },
  function: {
    type: String
  },
  password: {
    type: String,
    required: true
  }
});

const User = module.exports = mongoose.model('User', UserSchema);


module.exports.getAllUsers = function(callback) {
  User.find(callback);
}

module.exports.getUserById = function(id, callback) {
  User.findById(id, callback);
}

module.exports.getUserByUsername = function(username, callback) {
  const query = {username: username}
  User.findOne(query, callback);
}

module.exports.getUserByEmail = function(email, callback) {
  const query = {email: email}
  User.findOne(query, callback);
}

module.exports.addUser = function(newUser, callback){
  User.getUserByUsername(newUser.username, (err, user) => {
    if (user) {
      callback('Username already in use', null);
    } else {
      User.getUserByEmail(newUser.email, (err, user) => {
        if(user) {
          callback('Email adress already in use', null);
        } else {
          newUser._id = new mongoose.Types.ObjectId;
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
              if(err) throw err;
              newUser.password = hash;
              newUser.save(callback);
            });
          });
        }
      });
    }
  });
}

module.exports.updateUser = function(user, newUser, callback){
  if (user){
    User.getUserByUsername(newUser.username, (err, otherUser) => {
      if (otherUser && otherUser._id != newUser._id) {
        callback('Username already in use', null);
      } else {
        User.getUserByEmail(newUser.email, (err, otherUser) => {
          if (otherUser && otherUser._id != newUser._id) {
            callback('Email adress already in use', null);
          } else {
            user.firstname = newUser.firstname;
            user.lastname = newUser.lastname;
            user.organism = newUser.organism;
            user.function = user.function;
            user.username = newUser.username;
            user.email = newUser.email;
            user.save(callback);
          }
        });
      }
    });
  } else{
    callback('User not found',null);
  }
}

module.exports.updatePassword = function(user, newPassword, callback){
  if(user) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newPassword, salt, (err, hash) => {
        if(err) throw err;
        user.password = hash;
        user.save(callback);
      });
    });
  } else {
    callback('User not found',null);
  }
}

module.exports.comparePassword = function(candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  });
}
