# node.js authentication API

API for registering users with mongodb and authenticating using a json web token (JWT). This app uses passport and passport-jwt and uses a JWT strategy.

## Usage
```
npm install
```

```
npm start
```
or
```
nodemon
```

## Endpoints
To test the api, you can use Postman and create post/get requests using these adresses:
http://localhost:8080/endpoint

Specify a header `"Content-type" "application/json" ` and the body of the request.

```
POST /users/register // register a user in the mongodb database

body form:
{
	"name":"",
	"email":"",
	"username":"",
	"password":""
}
```

```
POST /users/authenticate // Gives back a json web token

body:
{
  "username":"",
  "password":""
}
```

```
GET /users/profile // needs a json web token to authorize

header : "Authorization" "<JWT token>"
```
